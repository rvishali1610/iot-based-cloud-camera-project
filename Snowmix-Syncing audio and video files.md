Myself and my teammate has done research to get a way to sync audio and video files. Here is one of the tool named Snowmix to sync audio and video files

# **Snowmix**

This is a tool to sync and overlay audio and video files. 

### Video Guide

Snowmix gets video input from video feeds through shared memory.
Video feeds - implemented using GStreamer module shmsrc(source), recieve from GStreamer module shmsink(destination).
GStreamer module needs a control channel to communicate with snowmix. That control channel is called pipe

**GStreamer shmsink** -  
- allocate shared memory area for the files.
- feed video frames to shared memory.
- signals the reciever that frames are ready to use.
- after the frames being used signals back to shmsink enabling the module to reuse the frame


The shmsink sends the frames in the format BGRA (Blue, Green, Red, Alpha)
![Click here](https://sourceforge.net/p/snowmix/wiki/Gstreamer/attachment/Snowmix-input.png)

Below is the example for inputting video files from GStreamer from a external IP based web camera using a MPEG-4 stream sent to a UDP port on the computer 
  
      #!/bin/bash

        # These settings are from the Snowmix ini file for the feed.

        CONTROL_PIPE=/tmp/feed1-control-pipe

        width=704

        height=576

        framerate='25/1'

        which gst-launch-1.0 2>/dev/null 1>&2

        if [ $? -eq 0 ] ; then

          gstlaunch=gst-launch-1.0

          SHMSIZE='shm-size='`echo "$width * $height * 4 * 22"|bc`

          MIXERFORMAT='video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=progressive'

          SCALENRATE='videoconvert ! videorate ! videoscale ! videoconvert'

        else

          gstlaunch=gst-launch-0.10

          SHMSIZE='shm-size='`echo "$width * $height * 4 * 8"|bc`

          MIXERFORMAT='video/x-raw-rgb,bpp=32,depth=32,endianness=4321,red_mask=65280,green_mask=16711680,"

          MIXERFORMAT=$MIXERFORMAT'blue_mask=-16777216,pixel-aspect-ratio=1/1,interlaced=false'

          SCALENRATE='ffmpegcolorspace ! videorate ! videoscale ! ffmpegcolorspace'

        fi

        CAPS="application/x-rtp,media=video,payload=96,clock-rate=90000,encoding-name=MP4V-ES"

        SRC=" udpsrc port=4444 caps=$CAPS ! rtpmp4vdepay ! mpeg4videoparse ! ffdec_mpeg4"

        SHMOPTION="wait-for-connection=0 sync=true "

        SHMSINK1="shmsink socket-path=$CONTROL_PIPE $SHMSIZE $SHMOPTION"

        while true ; do

            # Remove the named pipe if it exist

            rm -f $CONTROL_PIPE

            $gstlaunch -v           \

                $SRC               !\

                $SCALENRATE        !\

                "$MIXERFORMAT,framerate=$framerate,width=$width,height=$height" !\

                $SHMSINK1

            sleep 2

        done

        exit

In the above example 'filesrc' reads from the file $SNOWMIX/test/LES_TDS.mp4,decodes it, scales it,  and converts it to 32 bit BGRA before writing it to shared memory and signaling to Snowmix via the named pipe '/tmp/feed1-control-pipe'.And the video feeds can also be inputted from file source or from web camera

Below is the entries to be added to snowmix or to snowmix ini file to recieve the video feeds

        system control port 9999

        system geometry 1024 576 BGRA

        system frame rate 24

        system socket /tmp/mixer1

        feed add 1 Camera 1

        feed geometry 1 704 576

        feed live 1

        feed idle 1 100 frames/dead-704x576.bgra

        feed socket 1 /tmp/feed1-control-pipe

The  above entries added a feed 1 named Camera 1 and is defined by the geometry 704576 and it is live . The pipe used here is /tmp/feed1-control-pipe


### Audio Guide

This is done using 3 key components called audio feed, audio mixer and audio sink

#### Audio feed

The below command creates a audio feed

    audio feed add [<feed id> [<feed name>]]

The <feed id> in above command is a positive integer. If the id is 0 , it is reserved later for a special purpose.

The command to specify the port used

    system port <port number>

 The audio feeds were read  . The feeds read are queued ifatleast one active audio mixer and one active audio sink is sourced by audio feed. Otherwise the feeds are dropped. The feeds are sent to audio mixer.

 To start audio feeds

    audio feed ctr isaudio <feed id>

#### Audio mixer

The below command creates a audio mixer

    audio mixer add [<mixer id> [<mixer name>]]

The mixer id is a positive integer. If the id is 0 , it is reserved later for a special purpose. The feeds are then sent to audio sink.

To start audio mixer

    audio mixer start [[soft ]<mixer id>]

#### Audio sink

The below command creates a audio sink

    audio sink add [<sink id> [<sink name>]]

The sink id is a positive integer . If the id is 0, it is a special purpose sink which needs no file or control connection. It can either write to file or control connection and not both at the same time.

To start audio sink, either by command audio sink start or audio sink ctr isaudio

    audio sink start [<sink id>]
    audio sink ctr isaudio <sink id>

Audio feed and sink has a format of 16 bit signed/unsigned number. Audio mixers internally has 32 bit and truncated to 16 bit when sent to other audio components

There are several states
- **SETUP** - when audio feed,mixer,sinks are configured
- **READY** - when sufficient configuration has been applied individually
- **PENDING** - when individually started
- **RUNNING** - when data flows through the components

 ### Syncing audio and video feeds
 
 It gets input from its shared memory control sockets and its control connections. The input will come in an asynchronous way i.e.. not at the same time. When snowmix detects it, it executes 3 steps.
1.  If video frame through shared memory for feed input is ready, the frames are queued in FIFO.
1.  If video frame through shared memory for output has been set externally ready for reuse, it is added to the list of unused frames for later use.
1. Control connections contain commands and audio data.  If input data is ready , it will recieve the incoming audio data in its queue for the specific specific audio feed sourcing it

#### Manual sync
![](https://i.imgur.com/wb2zuXc.png)
1. Use clapper to verify that audio is ahead of video.
1. Add 10-20 ms of silence on feed or source in mixer.
1. Repeat step 2 until audio appear to be in sync with video
1. Add 10-20 ms of silence on feed or source in mixer.
1. Repeat step 4 until video is clearly ahead of audio

#### Automatic sync
If a camera and microphone is recoding a stream from Snowmix perhaps played on a laptop with both audio and video and the recoded audio and video stream is sent to Snowmix, it is possible to write a script for Snowmix that automatically determine the relative and absolute delay for both audio and video and subsequently determine the necessary audio delay needed for audio and video from the camera to be in sync in Snowmix.

Such a script is being developed as a Snowmix library (slib), but currently the script is hard to use and not supported.

### References
1. [Click here to know more about video guide](https://snowmix.sourceforge.io/Examples/input.html)
1. [Click here to know more about audio guide](https://sourceforge.net/p/snowmix/wiki/Audio/)
1. [Click here to know more about syncing audio and video](https://sourceforge.net/p/snowmix/wiki/AV%20Sync/)
